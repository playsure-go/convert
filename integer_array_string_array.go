package convert

func StringArrayToUint64Array(ss []string) ([]uint64, error) {
	var uis []uint64
	for _, s := range ss {
		ui, err := StringToUint64(s)
		if err != nil {
			return nil, err
		}
		uis = append(uis, ui)
	}
	return uis, nil
}

func StringArrayToUint32Array(ss []string) ([]uint32, error) {
	var uis []uint32
	for _, s := range ss {
		ui, err := StringToUint32(s)
		if err != nil {
			return nil, err
		}
		uis = append(uis, ui)
	}
	return uis, nil
}

func StringArrayToUint16Array(ss []string) ([]uint16, error) {
	var uis []uint16
	for _, s := range ss {
		ui, err := StringToUint16(s)
		if err != nil {
			return nil, err
		}
		uis = append(uis, ui)
	}
	return uis, nil
}

func StringArrayToUint8Array(ss []string) ([]uint8, error) {
	var uis []uint8
	for _, s := range ss {
		ui, err := StringToUint8(s)
		if err != nil {
			return nil, err
		}
		uis = append(uis, ui)
	}
	return uis, nil
}

func StringArrayToUintArray(ss []string) ([]uint, error) {
	var uis []uint
	for _, s := range ss {
		ui, err := StringToUint(s)
		if err != nil {
			return nil, err
		}
		uis = append(uis, ui)
	}
	return uis, nil
}

func StringArrayToInt64Array(ss []string) ([]int64, error) {
	var is []int64
	for _, s := range ss {
		ui, err := StringToInt64(s)
		if err != nil {
			return nil, err
		}
		is = append(is, ui)
	}
	return is, nil
}

func StringArrayToInt32Array(ss []string) ([]int32, error) {
	var is []int32
	for _, s := range ss {
		ui, err := StringToInt32(s)
		if err != nil {
			return nil, err
		}
		is = append(is, ui)
	}
	return is, nil
}

func StringArrayToInt16Array(ss []string) ([]int16, error) {
	var is []int16
	for _, s := range ss {
		ui, err := StringToInt16(s)
		if err != nil {
			return nil, err
		}
		is = append(is, ui)
	}
	return is, nil
}

func StringArrayToInt8Array(ss []string) ([]int8, error) {
	var is []int8
	for _, s := range ss {
		ui, err := StringToInt8(s)
		if err != nil {
			return nil, err
		}
		is = append(is, ui)
	}
	return is, nil
}

func StringArrayToIntArray(ss []string) ([]int, error) {
	var is []int
	for _, s := range ss {
		ui, err := StringToInt(s)
		if err != nil {
			return nil, err
		}
		is = append(is, ui)
	}
	return is, nil
}

func Uint64ArrayToStringArray(uis []uint64) []string {
	var ss []string
	for _, ui := range uis {
		s := Uint64ToString(ui)
		ss = append(ss, s)
	}
	return ss
}

func Uint32ArrayToStringArray(uis []uint32) []string {
	var ss []string
	for _, ui := range uis {
		s := Uint32ToString(ui)
		ss = append(ss, s)
	}
	return ss
}

func Uint16ArrayToStringArray(uis []uint16) []string {
	var ss []string
	for _, ui := range uis {
		s := Uint16ToString(ui)
		ss = append(ss, s)
	}
	return ss
}

func Uint8ArrayToStringArray(uis []uint8) []string {
	var ss []string
	for _, ui := range uis {
		s := Uint8ToString(ui)
		ss = append(ss, s)
	}
	return ss
}

func UintArrayToStringArray(uis []uint) []string {
	var ss []string
	for _, ui := range uis {
		s := UintToString(ui)
		ss = append(ss, s)
	}
	return ss
}

func Int64ArrayToStringArray(is []int64) []string {
	var ss []string
	for _, i := range is {
		s := Int64ToString(i)
		ss = append(ss, s)
	}
	return ss
}

func Int32ArrayToStringArray(is []int32) []string {
	var ss []string
	for _, i := range is {
		s := Int32ToString(i)
		ss = append(ss, s)
	}
	return ss
}

func Int16ArrayToStringArray(is []int16) []string {
	var ss []string
	for _, i := range is {
		s := Int16ToString(i)
		ss = append(ss, s)
	}
	return ss
}

func Int8ArrayToStringArray(is []int8) []string {
	var ss []string
	for _, i := range is {
		s := Int8ToString(i)
		ss = append(ss, s)
	}
	return ss
}

func IntArrayToStringArray(is []int) []string {
	var ss []string
	for _, i := range is {
		s := IntToString(i)
		ss = append(ss, s)
	}
	return ss
}
