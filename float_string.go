package convert

import "strconv"

func StringToFloat64(s string) (float64, error) {
	return strconv.ParseFloat(s, 64)
}

func StringToFloat32(s string) (float32, error) {
	f, e := strconv.ParseFloat(s, 32)
	return float32(f), e
}

func Float64ToString(f float64) string {
	return strconv.FormatFloat(f, 'f', -1, 10)
}

func Float32ToString(f float32) string {
	return strconv.FormatFloat(float64(f), 'f', -1, 10)
}
