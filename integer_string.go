package convert

import "strconv"

func StringToUint64(s string) (uint64, error) {
	return strconv.ParseUint(s, 10, 64)
}

func StringToUint32(s string) (uint32, error) {
	u, e := strconv.ParseUint(s, 10, 32)
	return uint32(u), e
}

func StringToUint16(s string) (uint16, error) {
	u, e := strconv.ParseUint(s, 10, 16)
	return uint16(u), e
}

func StringToUint8(s string) (uint8, error) {
	u, e := strconv.ParseUint(s, 10, 8)
	return uint8(u), e
}

func StringToUint(s string) (uint, error) {
	u, e := strconv.ParseUint(s, 10, 64)
	return uint(u), e
}

func StringToInt64(s string) (int64, error) {
	return strconv.ParseInt(s, 10, 64)
}

func StringToInt32(s string) (int32, error) {
	i, e := strconv.ParseInt(s, 10, 32)
	return int32(i), e
}

func StringToInt16(s string) (int16, error) {
	i, e := strconv.ParseInt(s, 10, 16)
	return int16(i), e
}

func StringToInt8(s string) (int8, error) {
	i, e := strconv.ParseInt(s, 10, 8)
	return int8(i), e
}

func StringToInt(s string) (int, error) {
	i, e := strconv.ParseInt(s, 10, 64)
	return int(i), e
}

func Uint64ToString(u uint64) string {
	return strconv.FormatUint(u, 10)
}

func Uint32ToString(u uint32) string {
	return strconv.FormatUint(uint64(u), 10)
}

func Uint16ToString(u uint16) string {
	return strconv.FormatUint(uint64(u), 10)
}

func Uint8ToString(u uint8) string {
	return strconv.FormatUint(uint64(u), 10)
}

func UintToString(u uint) string {
	return strconv.FormatUint(uint64(u), 10)
}

func Int64ToString(i int64) string {
	return strconv.FormatInt(i, 10)
}

func Int32ToString(i int32) string {
	return strconv.FormatInt(int64(i), 10)
}

func Int16ToString(i int16) string {
	return strconv.FormatInt(int64(i), 10)
}

func Int8ToString(i int8) string {
	return strconv.FormatInt(int64(i), 10)
}

func IntToString(i int) string {
	return strconv.FormatInt(int64(i), 10)
}
