package convert

import "strings"

func StringSplitToUint64Array(s string, sep string) ([]uint64, error) {
	ss := strings.Split(s, sep)
	return StringArrayToUint64Array(ss)
}

func StringSplitToUint32Array(s string, sep string) ([]uint32, error) {
	ss := strings.Split(s, sep)
	return StringArrayToUint32Array(ss)
}

func StringSplitToUint16Array(s string, sep string) ([]uint16, error) {
	ss := strings.Split(s, sep)
	return StringArrayToUint16Array(ss)
}

func StringSplitToUint8Array(s string, sep string) ([]uint8, error) {
	ss := strings.Split(s, sep)
	return StringArrayToUint8Array(ss)
}

func StringSplitToUintArray(s string, sep string) ([]uint, error) {
	ss := strings.Split(s, sep)
	return StringArrayToUintArray(ss)
}

func StringSplitToInt64Array(s string, sep string) ([]int64, error) {
	ss := strings.Split(s, sep)
	return StringArrayToInt64Array(ss)
}

func StringSplitToInt32Array(s string, sep string) ([]int32, error) {
	ss := strings.Split(s, sep)
	return StringArrayToInt32Array(ss)
}

func StringSplitToInt16Array(s string, sep string) ([]int16, error) {
	ss := strings.Split(s, sep)
	return StringArrayToInt16Array(ss)
}

func StringSplitToInt8Array(s string, sep string) ([]int8, error) {
	ss := strings.Split(s, sep)
	return StringArrayToInt8Array(ss)
}

func StringSplitToIntArray(s string, sep string) ([]int, error) {
	ss := strings.Split(s, sep)
	return StringArrayToIntArray(ss)
}

func Uint64ArrayJoinToString(uis []uint64, sep string) string {
	ss := Uint64ArrayToStringArray(uis)
	return strings.Join(ss, sep)
}

func Uint32ArrayJoinToString(uis []uint32, sep string) string {
	ss := Uint32ArrayToStringArray(uis)
	return strings.Join(ss, sep)
}

func Uint16ArrayJoinToString(uis []uint16, sep string) string {
	ss := Uint16ArrayToStringArray(uis)
	return strings.Join(ss, sep)
}

func Uint8ArrayJoinToString(uis []uint8, sep string) string {
	ss := Uint8ArrayToStringArray(uis)
	return strings.Join(ss, sep)
}

func UintArrayJoinToString(uis []uint, sep string) string {
	ss := UintArrayToStringArray(uis)
	return strings.Join(ss, sep)
}

func Int64ArrayJoinToString(is []int64, sep string) string {
	ss := Int64ArrayToStringArray(is)
	return strings.Join(ss, sep)
}

func Int32ArrayJoinToString(is []int32, sep string) string {
	ss := Int32ArrayToStringArray(is)
	return strings.Join(ss, sep)
}

func Int16ArrayJoinToString(is []int16, sep string) string {
	ss := Int16ArrayToStringArray(is)
	return strings.Join(ss, sep)
}

func Int8ArrayJoinToString(is []int8, sep string) string {
	ss := Int8ArrayToStringArray(is)
	return strings.Join(ss, sep)
}

func IntArrayJoinToString(is []int, sep string) string {
	ss := IntArrayToStringArray(is)
	return strings.Join(ss, sep)
}
