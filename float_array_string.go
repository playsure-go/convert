package convert

import "strings"

func StringSplitToFloat64Array(s string, sep string) ([]float64, error) {
	ss := strings.Split(s, sep)
	return StringArrayToFloat64Array(ss)
}

func StringSplitToFloat32Array(s string, sep string) ([]float32, error) {
	ss := strings.Split(s, sep)
	return StringArrayToFloat32Array(ss)
}

func Float64ArrayJoinToString(fs []float64, sep string) string {
	ss := Float64ArrayToStringArray(fs)
	return strings.Join(ss, sep)
}

func Float32ArrayJoinToString(fs []float32, sep string) string {
	ss := Float32ArrayToStringArray(fs)
	return strings.Join(ss, sep)
}
