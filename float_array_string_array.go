package convert

func StringArrayToFloat64Array(ss []string) ([]float64, error) {
	var fs []float64
	for _, s := range ss {
		f, err := StringToFloat64(s)
		if err != nil {
			return nil, err
		}
		fs = append(fs, f)
	}
	return fs, nil
}

func StringArrayToFloat32Array(ss []string) ([]float32, error) {
	var fs []float32
	for _, s := range ss {
		f, err := StringToFloat32(s)
		if err != nil {
			return nil, err
		}
		fs = append(fs, f)
	}
	return fs, nil
}

func Float64ArrayToStringArray(fs []float64) []string {
	var ss []string
	for _, f := range fs {
		s := Float64ToString(f)
		ss = append(ss, s)
	}
	return ss
}

func Float32ArrayToStringArray(fs []float32) []string {
	var ss []string
	for _, f := range fs {
		s := Float32ToString(f)
		ss = append(ss, s)
	}
	return ss
}
