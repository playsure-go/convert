package convert

import (
	"encoding/hex"
	"strconv"
)

func ByteToHexString(b byte) string {
	s := strconv.FormatUint(uint64(b), 16)
	if b < 16 {
		s = "0" + s
	}
	return s
}

func HexStringToByte(s string) (byte, error) {
	u, e := strconv.ParseUint(s, 16, 8)
	return byte(u), e
}

func ByteArrayToHexString(bs []byte) string {
	return hex.EncodeToString(bs)
}

func HexStringToByteArray(s string) ([]byte, error) {
	return hex.DecodeString(s)
}
